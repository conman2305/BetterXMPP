package net.angrycoding.betterxmpp.message;

import com.google.gson.Gson;

import java.util.HashMap;

/**
 * Created by conma on 7/1/2015.
 */
public class Message<D> {

    /**
     * The actual data this message is holding
     */
    private D dataField;

    /**
     * The type of message this is
     */
    private MessageType type;

    /**
     * Metadata about the message
     */
    private HashMap<String, Object> metaData;

    /**
     *
     * @param dataField The data this message should have
     * @param type The type of message this is
     */
    public Message(D dataField, MessageType type) {
        this.dataField = dataField;
        this.type = type;
    }

    public void addMetadata(String key, Object data) {
        metaData.put(key, data);
    }
}
