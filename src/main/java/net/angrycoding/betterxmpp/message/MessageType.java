package net.angrycoding.betterxmpp.message;

/**
 * Created by conma on 7/1/2015.
 */
public enum MessageType {
    CHAT_MESSAGE, CONTROL_MESSAGE
}
