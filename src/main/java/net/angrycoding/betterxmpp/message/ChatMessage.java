package net.angrycoding.betterxmpp.message;

import java.util.Base64;

/**
 * Created by conma on 7/1/2015.
 */
public class ChatMessage {

    /**
     * Who this message is from
     */
    private String fromField;

    /**
     * Who this message is to
     */
    private String toField;

    /**
     * The message contents
     */
    private String message;

    /**
     *
     * @param to The user to send the message to
     * @param from The user the message came from
     * @param msg The message contents
     */
    public ChatMessage(String to, String from, String msg) {
        fromField = from;
        toField = to;
        message = msg;
    }

    /**
     *
     * @param to The user to send the message to
     * @param from The user the message came from
     * @param data Binary data (will be encoded as base64)
     */
    public ChatMessage(String to, String from, byte[] data) {
        fromField = from;
        toField = to;
        message = Base64.getEncoder().encodeToString(data);
    }

    public String getMessageText() {
        return message;
    }

    public String getFromField() {
        return fromField;
    }

    public String getToField() {
        return toField;
    }
}
