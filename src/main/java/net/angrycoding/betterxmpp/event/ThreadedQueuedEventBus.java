package net.angrycoding.betterxmpp.event;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by conma on 7/7/2015.
 */
public class ThreadedQueuedEventBus {
    private ConcurrentLinkedQueue<Event> eventQueue = new ConcurrentLinkedQueue<>();
    private ConcurrentLinkedQueue<EventListener> listeners = new ConcurrentLinkedQueue<>();

    public void fireEvent(Event e) {
        eventQueue.add(e);
    }

    public void addEventListener(EventListener e) {
        listeners.add(e);
    }

    private static void eventBus(ConcurrentLinkedQueue<Event> eventQueue,
                                 ConcurrentLinkedQueue<EventListener> listeners) {

        while (true) {
            if(eventQueue.size() > 0)
                listeners.forEach(listener -> listener.processEvent(eventQueue.remove()));
        }
    }

    public ThreadedQueuedEventBus() {
        Thread queueDrainThread = new Thread(() -> { eventBus(eventQueue, listeners); });
        queueDrainThread.start();
    }
}
