package net.angrycoding.betterxmpp.event;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Connor B. Reed
 *
 * Base class representing an event. This is not meant to be instantiated.
 */
public abstract class Event <D> {

    /**
     * The data object this event contains
     */
    protected D dataObject;

    /**
     * Any metadata associated with this event
     */
    protected ConcurrentHashMap<String, Object> metaData;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event<?> event = (Event<?>) o;

        if (!dataObject.equals(event.dataObject)) return false;
        return !(metaData != null ? !metaData.equals(event.metaData) : event.metaData != null);

    }

    @Override
    public int hashCode() {
        int result = dataObject.hashCode();
        result = 31 * result + (metaData != null ? metaData.hashCode() : 0);
        return result;
    }

}
