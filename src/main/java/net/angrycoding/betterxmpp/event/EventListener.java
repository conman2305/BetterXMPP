package net.angrycoding.betterxmpp.event;

/**
 * Created by conma on 7/7/2015.
 */
public interface EventListener {
    //TODO: Handle Failure
    boolean processEvent(Event e);
}
