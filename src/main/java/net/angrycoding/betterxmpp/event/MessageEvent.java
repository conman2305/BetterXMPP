package net.angrycoding.betterxmpp.event;

import net.angrycoding.betterxmpp.message.ChatMessage;

/**
 * Created by conma on 7/7/2015.
 */
public class MessageEvent extends Event<ChatMessage> {

    public MessageEvent(String to, String from, String message) {
        dataObject = new ChatMessage(to, from, message);
    }

    public MessageEvent(ChatMessage chatMsg) {
        dataObject = chatMsg;
    }
    public MessageEvent(String to, String from, byte[] data) {
        dataObject = new ChatMessage(to, from, data);
    }

    public void addMetaData(String key, Object value) {
        metaData.put(key, value);
    }

    public Object getMetaData(String key) {
        return metaData.get(key);
    }

    public ChatMessage getChatMessage() {
        return dataObject;
    }
}
