package net.angrycoding.betterxmpp;

import com.google.gson.Gson;
import net.angrycoding.betterxmpp.event.MessageEvent;
import net.angrycoding.betterxmpp.event.ThreadedQueuedEventBus;
import net.angrycoding.betterxmpp.message.ChatMessage;
import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;

import java.io.IOException;

/**
 * Created by conma on 7/7/2015.
 */
public class BetterXMPP {

    private XMPPTCPConnectionConfiguration config;
    private AbstractXMPPConnection connection;
    private Roster roster;
    private ChatManager chatManager;
    private ThreadedQueuedEventBus rxBus = new ThreadedQueuedEventBus();
    private ThreadedQueuedEventBus txBus = new ThreadedQueuedEventBus();
    private ThreadedQueuedEventBus ctlBus = new ThreadedQueuedEventBus();

    public BetterXMPP(XMPPTCPConnectionConfiguration conf) throws IOException, XMPPException, SmackException {
        config = conf;

        connection = new XMPPTCPConnection(config);

        connection.connect();
        connection.login();

        roster = Roster.getInstanceFor(connection);
        roster.reloadAndWait();

        Session.getInstance().setCurrentUser(config.getUsername().toString());

        chatManager = ChatManager.getInstanceFor(connection);
        chatManager.addChatListener(
                (chat, createdLocally) -> {
                    chat.addMessageListener((recvChat, message) -> {
                        if (message.getBody().contains("{")) {
                            final Gson gson = new Gson();
                            MessageEvent e = gson.fromJson(message.getBody(), MessageEvent.class);
                            rxBus.fireEvent(e);
                        } else {
                            MessageEvent e = new MessageEvent(Session.getInstance().getCurrentUser(), recvChat.getParticipant(), message.getBody());
                            rxBus.fireEvent(e);
                        }

                    });
                });
        txBus.addEventListener(e -> {
            if(e instanceof MessageEvent) {
                MessageEvent msgEvnt = (MessageEvent) e;
                final Gson gson = new Gson();
                try {
                    chatManager.createChat(msgEvnt.getChatMessage().getToField()).sendMessage(gson.toJson(msgEvnt));
                } catch (SmackException.NotConnectedException e1) {
                    e1.printStackTrace();
                    //TODO: Send control message that we aren't connected.
                    return false;
                }

                return true;
            }
            return false;
        });
    }


    public void startEventEngine() {
        while(true) {
            //TODO: Process monitoring and heartbeat stuff. For now just block forever.
        }
    }
    public Roster getRoster() {
        return roster;
    }

    public void sendMessage(ChatMessage message) {
        MessageEvent msgEvnt = new MessageEvent(message);
        txBus.fireEvent(msgEvnt);
    }

    public void sendMessage(String to, String message) {
        MessageEvent msgEvnt = new MessageEvent(to, Session.getInstance().getCurrentUser(), message);
        txBus.fireEvent(msgEvnt);
    }

    public void sendLegacyMessage(ChatMessage message) {
        try {
            chatManager.createChat(message.getToField()).sendMessage(message.getMessageText());
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
            //TODO: Deal with this better.
            System.exit(1);
        }
    }
    public ThreadedQueuedEventBus getCtlBus() {
        return ctlBus;
    }

    public ThreadedQueuedEventBus getRxBus() {
        return rxBus;
    }

    public ThreadedQueuedEventBus getTxBus() {
        return txBus;
    }
}
