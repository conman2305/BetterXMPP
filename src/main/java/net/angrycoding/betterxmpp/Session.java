package net.angrycoding.betterxmpp;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Thread-safe implementation of a central session class for storing information.
 */
public class Session {

    /**
     * HashMap to store miscellaneous session data
     */
    private static ConcurrentHashMap<String, Object> sessionData = new ConcurrentHashMap<>();

    /**
     * Our singleton instance
     */
    private static Session session = new Session();


    private Session() {
    }

    /**
     * Returns the current instance of the session. This is synchronized so it is thread-safe.
     * @return The current session instance
     */
    public synchronized static Session getInstance() {
        return session;
    }

    /**
     * Adds a new session object. This is also thread-safe as the underlying HashMap is a ConcurrentHashMap.
     * @param key The key to add the entry at.
     * @param value The value to store.
     */
    public void addSessionObject(String key, Object value) {
        sessionData.put(key, value);
    }

    /**
     * Retrieves a previously stored value.
     * @param key The key to retrieve.
     * @return The object (if it exists - will return null if it doesn't)
     */
    public Object getSessionData(String key) {
        return sessionData.get(key);
    }

    public String getCurrentUser() {
        return (String) getSessionData("_user");
    }

    public void setCurrentUser(String username) {
        addSessionObject("_user", username);
    }
}
