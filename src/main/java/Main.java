import net.angrycoding.betterxmpp.BetterXMPP;
import net.angrycoding.betterxmpp.Session;
import net.angrycoding.betterxmpp.event.MessageEvent;
import net.angrycoding.betterxmpp.message.ChatMessage;
import org.jivesoftware.smack.*;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;
import org.jivesoftware.smack.roster.RosterGroup;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;

import javax.net.ssl.SSLSocketFactory;
import java.io.IOException;
import java.util.Collection;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by conma on 6/27/2015.
 */
public class Main {

   public static void main(String[] args) {

       Scanner console = new Scanner(System.in);
       System.out.print("Enter Username: ");
       String user = console.nextLine();
       System.out.print("Enter Password: ");
       String pass = console.nextLine();
       // Create a connection to the jabber.org server on a specific port.
       XMPPTCPConnectionConfiguration config = XMPPTCPConnectionConfiguration.builder()
               .setUsernameAndPassword(user, pass)
               .setServiceName("oracle.com")
//               .setSecurityMode(ConnectionConfiguration.SecurityMode.required)
               .setHost("stbeehive.oracle.com")
               .setResource("WIN-6986JI5AVDU")
               .setPort(5223)
               .setSocketFactory(SSLSocketFactory.getDefault())
               .setDebuggerEnabled(true)
               .build();

       try {
           BetterXMPP b = new BetterXMPP(config);
           Thread queueDrainThread = new Thread(() -> { b.startEventEngine(); });
           queueDrainThread.start();
           b.getRxBus().addEventListener(e -> {
               if (e instanceof MessageEvent) {
                   MessageEvent msgEvnt = (MessageEvent) e;
                   ChatMessage msg = msgEvnt.getChatMessage();
                   System.out.println(msg.getFromField() + ": " + msg.getMessageText());
                   return true;
               }

               return false;
           });

//           b.sendMessage("dvci-notifications_us@oracle.com", "operator help");
           ChatMessage msg = new ChatMessage("dvci-notifications_us@oracle.com", Session.getInstance().getCurrentUser(), "operator help");
           b.sendLegacyMessage(msg);
           while (true) { }

       } catch (IOException e) {
           e.printStackTrace();
           System.exit(1);
       } catch (XMPPException e) {
           e.printStackTrace();
           System.exit(1);
       } catch (SmackException e) {
           e.printStackTrace();
           System.exit(1);
       }

       /*AbstractXMPPConnection conn2 = new XMPPTCPConnection(config);
       try {
           conn2.connect();
       } catch (SmackException e) {
           e.printStackTrace();
       } catch (IOException e) {
           e.printStackTrace();
       } catch (XMPPException e) {
           e.printStackTrace();
       }

       try {
           conn2.login();
       } catch (XMPPException e) {
           e.printStackTrace();
       } catch (SmackException e) {
           e.printStackTrace();
       } catch (IOException e) {
           e.printStackTrace();
       }
       Roster r = Roster.getInstanceFor(conn2);
       try {
           r.reloadAndWait();
       } catch (SmackException.NotLoggedInException e) {
           e.printStackTrace();
       } catch (SmackException.NotConnectedException e) {
           e.printStackTrace();
       }
       Collection<RosterEntry> rosterEntrySet = r.getEntries();
       //rosterEntrySet.addAll(r.getUnfiledEntries());
       for(RosterEntry entry : rosterEntrySet)
       {
           System.out.println("Roster Entry: " + entry.getName());
       }

       ChatManager chatManager = ChatManager.getInstanceFor(conn2);
       chatManager.addChatListener(
               (chat, createdLocally) -> {
                   //if (!createdLocally)
                       chat.addMessageListener((recvChat, message) -> {
                           try {
                               recvChat.sendMessage("operator help");
                               System.out.println("Participant: " + recvChat.getParticipant());
                           } catch (SmackException.NotConnectedException e) {
                               e.printStackTrace();
                           }
                       });
               });
       Chat newChat = chatManager.createChat("dvci-notifications_us@oracle.com");
       try {
           newChat.sendMessage("operator help");
       } catch (SmackException.NotConnectedException e) {
           e.printStackTrace();
       }
       while (true)
       {

       }*/
       //conn2.disconnect();
   }
}
